#!/bin/bash
ENV=$1
MONGO_PASSWORD=$2
SQL_PASSWORD=$3
MS_NAME=$4
IMAGE_VERSION=$5

echo "In server to deploy ENV=${ENV}"

if [ "$(sudo docker images -q $MS_NAME:$IMAGE_VERSION)" ]; then
  sudo docker rmi registry.dkatalis.com/$MS_NAME:$IMAGE_VERSION #remove this part once we have implement gitsha
fi

sudo docker pull registry.dkatalis.com/$MS_NAME:$IMAGE_VERSION

if [ "$(sudo docker ps -q -f name=$MS_NAME)" ]; then
  sudo docker stop $MS_NAME && sudo docker rm $MS_NAME
fi

sudo docker run --restart always --network=host -d --name=$MS_NAME -e NODE_ENV=${ENV} -e MONGO_PASSWORD=${MONGO_PASSWORD} -e SQL_PASSWORD=${SQL_PASSWORD} registry.dkatalis.com/$MS_NAME:$IMAGE_VERSION
