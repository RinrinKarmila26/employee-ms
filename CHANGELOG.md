# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.2.16 (2020-02-25)

### 1.2.15 (2020-02-25)

### [1.2.14](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.14) (2020-02-24)

### [1.2.13](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.13) (2020-02-24)

### [1.2.12](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.12) (2020-02-24)

### [1.2.11](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.11) (2020-02-24)

### [1.2.10](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.10) (2020-02-24)

### [1.2.9](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.9) (2020-02-22)

### [1.2.8](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.8) (2020-02-21)

### [1.2.7](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.7) (2020-02-18)

### [1.2.6](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.6) (2020-02-17)

### [1.2.5](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.5) (2020-02-13)

### [1.2.4](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.4) (2020-02-13)

### [1.2.3](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.3) (2020-02-13)

### [1.2.2](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.2) (2020-02-11)

### [1.2.1](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.2.1) (2020-02-10)

### [1.1.6](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.1.6) (2020-02-10)

### [1.1.5](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.1.5) (2020-02-10)

### [1.1.4](http://gitlab.com/dk-digital-bank/seeds/seed-ms-node/compare/v1.0.2...v1.1.4) (2020-02-10)
