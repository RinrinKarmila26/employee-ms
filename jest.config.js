module.exports = {
  roots: ['<rootDir>/src'],
  setupFilesAfterEnv: ['<rootDir>/src/__jest__/setup.ts'],
  transform: {
    '^.+\\.ts$': 'ts-jest'
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/?(*.)(spec|test).ts'],
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  globals: {
    'ts-jest': {
      diagnostics: false
    }
  },
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0
    }
  },
  collectCoverageFrom: [
    '!src',
    'src/**/*.{ts,js}',
    '!src/**/*.(interface|constant).{ts,js}',
    '!**/__mocks__/**',
    '!**/node_modules/**'
  ]
};
