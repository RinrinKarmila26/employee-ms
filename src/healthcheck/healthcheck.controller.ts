import hapi from '@hapi/hapi';

const ping: hapi.ServerRoute = {
  method: 'GET',
  path: '/ping',
  options: {
    description: 'Pongs back',
    notes: 'To check is service pongs on a ping',
    tags: ['api'],
    handler: (_request: hapi.Request, _h: hapi.ResponseToolkit) => 'pong lala!'
  }
};

// const employeePut : hapi.ServerRoute = {
//   method: 'PUT',
//   path: '/tryput',
//   options: {
//     description: 'put response',
//     notes: 'Check for the method put',
//     tags: ['api'],
//     handler: (_request: hapi.Request, _h: hapi.ResponseToolkit) => {
//       'idEmploye' : 1 ,
//       'nameEmployee'

//     }
//   }
// };
const healthController: hapi.ServerRoute[] = [ping];
export default healthController;
