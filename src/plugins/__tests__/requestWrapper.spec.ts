import hapi from '@hapi/hapi';

import requestWrapper from '../requestWrapper';
import responseWrapper from '../responseWrapper';
import { Http } from '@dk/module-common/';

describe('Plugin - requestWrapper', () => {
  const testServer = new hapi.Server();
  testServer.route([
    {
      method: Http.Method.GET,
      path: '/test',
      options: {
        handler: () => 'test'
      }
    }
  ]);

  testServer.register([requestWrapper, responseWrapper]);

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('handleHapiRequest', () => {
    it('should continue with request made without any error', async () => {
      const response: hapi.ServerInjectResponse = await testServer.inject({
        method: Http.Method.GET,
        url: '/test'
      });
      expect(response.result.data).toEqual('test');
    });
  });
});
