import { fooService } from '../foo.services';
import sqlDb from '../../common/sqlDb';
import { getConnection } from 'typeorm';
import { FooEntity } from '../foo.entities';

describe('foo controller', () => {
  beforeAll(async () => {
    await sqlDb.init();

    const foo: FooEntity = {
      name: 'foo'
    };

    await getConnection()
      .getRepository(FooEntity)
      .insert(foo);
  });

  afterAll(async () => {
    await sqlDb.close();
  });

  it('should return all foos', async () => {
    const foos = await fooService.getFoos();
    expect(foos.length).toBeGreaterThan(0);
  });
});
