import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  createEmployeeRequestValidator,
  EmployeeResponseValidator,
  updateEmployeeRequestValidator
} from './employee.validator';
import {
  IEmployeeRequest
  // IEmployeeRequestUpdate,
  // IEmployeeRequestDelete
} from './employee.interface';

const getUser: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employees',
    notes: 'Get the list of employees',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};
const createUser: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employee',
  options: {
    description: 'Create new data employee',
    notes: 'All information must valid',
    validate: {
      payload: createEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employee'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'data employe created.'
          }
        }
      }
    }
  }
};

const updateDataEmployee: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/employee/update/{_id}',
  options: {
    description: 'update data employee',
    notes: 'data must valid',
    validate: {
      payload: updateEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api,', 'updateEmployee'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newData = await employeeService.updateEmployee(
        hapiRequest.payload,
        hapiRequest.params._id
      );
      return hapiResponse.response(newData).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'data employe created.'
          }
        }
      }
    }
  }
};

const deleteDataEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employee/delete/{_id}',
  options: {
    description: 'update data employee',
    notes: 'data must valid',
    validate: {
      payload: updateEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api,', 'updateEmployee'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newData = await employeeService.deleteEmploy(
        hapiRequest.params._id
      );
      return hapiResponse.response(newData).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'data employe created.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [
  getUser,
  createUser,
  updateDataEmployee,
  deleteDataEmployee
];
export default employeeController;
