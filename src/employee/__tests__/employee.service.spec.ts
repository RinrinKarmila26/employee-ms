import employeeService from '../employee.service';
import employeeRepository from '../employee.repository';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

jest.mock('../employee.repository');

describe('employeeService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getEmployee', () => {
    it('should return repository get result', async () => {
      const employTest = [
        {
          idEmploy: '231',
          nameEmployee: 'jayaaa',
          year: 2000,
          address: 'jakarta',
          phone: 0922123445,
          email: 'ghaha@gmail.com'
        }
      ];
      employeeRepository.get.mockResolvedValueOnce(employTest);
      const users = await employeeService.getEmployees();
      expect(users).toEqual(employTest);
    });
  });

  describe('createUser', () => {
    it('should throw error if repository find existing user with same name', async () => {
      const employTest = {
        idEmploy: '231',
        nameEmployee: 'jayaaa',
        year: 2000,
        address: 'jakarta',
        phone: 0922123445,
        email: 'ghaha@gmail.com'
      };
      employeeRepository.getFirstById.mockResolvedValueOnce({
        idEmploy: '231',
        nameEmployee: 'jayaaa',
        year: 2000,
        address: 'jakarta',
        phone: 0922123445,
        email: 'ghaha@gmail.com'
      });
      try {
        await employeeService.createEmployee(employTest);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_EXISTED);
      } finally {
        expect(employeeRepository.create).not.toBeCalled();
      }
    });

    it('should call repository create data employee', async () => {
      const employTest = {
        idEmploy: '231',
        nameEmployee: 'jayaaa',
        year: 2000,
        address: 'jakarta',
        phone: 0922123445,
        email: 'ghaha@gmail.com'
      };
      employeeRepository.getFirstById.mockResolvedValueOnce(null);
      await employeeService.createEmployee(employTest);
      expect(employeeRepository.create).toBeCalledWith(employTest);
    });
  });
});
