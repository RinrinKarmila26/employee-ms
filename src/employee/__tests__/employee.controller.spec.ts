import hapi = require('@hapi/hapi');

import employeeService from '../employee.service';
import employeeController from '../employee.controller';
import { IEmployee } from '../employee.interface';
jest.mock('../employee.service', () => ({
  getEmployees: jest.fn(),
  createEmployee: jest.fn()
}));
let server: hapi.Server;

describe('employee.controller', () => {
  beforeAll(async () => {
    server = new hapi.Server();
    server.route(employeeController);
  });

  describe('GET /employeess', () => {
    it('should return userService getUsers with code 200', async () => {
      const testEmployee = [
        {
          _id: '5d29b6716394dea3588023d4',
          idEmploy: '231',
          nameEmployee: 'jayaaa',
          year: 2000,
          address: 'jakarta',
          phone: 0922123445,
          email: 'ghaha@gmail.com',
          __v: 0,
          id: '5d29b6716394dea3588023d4'
        }
      ];
      employeeService.getEmployees.mockResolvedValueOnce(testEmployee);
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'GET',
        url: `/employee`
      });
      expect(result.statusCode).toBe(200);
      expect(result.result).toEqual(testEmployee);
      expect(employeeService.getEmployees).toHaveBeenCalledTimes(1);
    });
  });

  describe('POST /employees', () => {
    it('should return error on empty name', async () => {
      const testEmployee: IEmployee = {
        idEmploy: '',
        nameEmployee: 'abadi',
        year: 2003,
        address: '',
        phone: 0922123345,
        email: 'hajah@gmail.com'
      };
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/`,
        payload: testEmployee
      });

      expect(result.statusCode).toBe(400);
      expect(result.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return error on not enough phone', async () => {
      const testEmployee: IEmployee = {
        idEmploy: '657',
        nameEmployee: 'abadi',
        year: 2003,
        address: '',
        phone: 0925,
        email: 'hajah@gmail.com'
      };
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(400);
      expect(response.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return 201 on valid payload and success service call', async () => {
      const testEmployee: IEmployee = {
        idEmploy: '8993484',
        nameEmployee: 'abadi',
        year: 2003,
        address: '',
        phone: 0925,
        email: 'hajah@gmail.com'
      };

      employeeService.createEmployee.mockResolvedValueOnce({
        ...testEmployee,
        id: 'new id'
      });

      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(201);
      expect(response.result).toEqual({
        ...testEmployee,
        id: 'new id'
      });
    });
  });
});

describe('PUT/employee/update/{_id}', () => {
  it('should return 201 on valid payload and success service call', async () => {
    const newdata: IEmployee = {
      idEmploy: '899',
      nameEmployee: 'bintang',
      year: 2003,
      address: 'bandung',
      phone: 0925678638,
      email: 'lakauh@gmail.com'
    };

    employeeService.createEmployee.mockResolvedValueOnce({
      ...newdata,
      nameEmployee: 'new name'
    });

    const response: hapi.ServerInjectResponse = await server.inject({
      method: 'PUT',
      url: `/employees/update/{_id}`,
      payload: newdata
    });

    expect(response.statusCode).toBe(201);
    expect(response.result).toEqual({
      ...newdata,
      nameEmployee: 'new name'
    });
  });
});

describe('DELETE/employee/delete/{_id}', () => {
  it('should return 201 on valid payload and success service call', async () => {
    const newdata: IEmployee = {
      idEmploy: '899',
      nameEmployee: 'bintang',
      year: 2003,
      address: 'bandung',
      phone: 0925678638,
      email: 'lakauh@gmail.com'
    };

    employeeService.createEmployee.mockResolvedValueOnce({
      ...newdata,
      nameEmployee: 'new name'
    });

    const response: hapi.ServerInjectResponse = await server.inject({
      method: 'PUT',
      url: `/employees/update/{_id}`,
      payload: newdata
    });

    expect(response.statusCode).toBe(201);
    expect(response.result).toEqual({
      ...newdata,
      nameEmployee: 'new name'
    });
  });
});
