import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

import { EmployeeModel } from '../employee.model';
import employeeRepository from '../employee.repository';
// jest.setTimeout(60000); //untuk pertama kali,
jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('employee.repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterAll(async () => {
    mongoose.disconnect();
    mongod.stop();
  });

  describe('get', () => {
    it('should get all employee', async () => {
      await EmployeeModel.create(
        {
          nameEmployee: 'u1'
        },
        {
          nameEmployee: 'u2'
        }
      );

      const employs = await employeeRepository.get();
      expect(employs).toHaveLength(2);
    });
  });

  describe('create', () => {
    it('should create new user', async () => {
      const newEmploy = await employeeRepository.create({
        idEmploy: '231',
        nameEmployee: 'jayaaa',
        year: 2000,
        address: 'jakarta',
        phone: 0922123445,
        email: 'ghaha@gmail.com'
        // id: '5d29b6716394dea3588023d4'
      });
      expect(newEmploy.id).toBeDefined();
    });
  });

  describe('getFirstById', () => {
    it('should return first employee if name exists in db', async () => {
      await EmployeeModel.create({
        nameEmployee: 'exist name'
      });
      const employ = await employeeRepository.getFirstById('exist name');
      expect(employ.id).toEqual('exist name');
    });

    it('should return null if name not exists in db', async () => {
      const employ = await employeeRepository.getFirstById('not exist name');
      expect(employ).toBeNull();
    });
  });
});

describe('update', () => {
  it('should update existing employee', async () => {
    const data = await employeeRepository.create({
      idEmploy: '231',
      nameEmployee: 'selaalu love',
      year: 2000,
      address: 'jakarta',
      phone: 0922123445,
      email: 'ghaha@gmail.com'
    });

    data.nameEmployee = ' name changed';

    const updatedEmployee = await employeeRepository.updateDataEmployee(
      data,
      data._id
    );

    expect(updatedEmployee.nameEmployee).toBe(data.nameEmployee);
  });
});

describe('update Delete', () => {
  it('should update existing employee', async () => {
    const oldData = await employeeRepository.create({
      idEmploy: '231',
      nameEmployee: 'jayaaa',
      year: 2000,
      address: 'jakarta',
      phone: 0922123445,
      email: 'ghaha@gmail.com'
    });

    const deletEmployee = await employeeRepository.deleteDataEmployee(
      oldData._id
    );
    const employeesdeleteByID = await EmployeeModel.findById(deletEmployee.id);

    expect(employeesdeleteByID).toBeNull();
  });
});
