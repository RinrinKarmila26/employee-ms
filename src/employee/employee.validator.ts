import * as Joi from '@hapi/joi';

import employeeConstant from './employee.constant';

import { MongooseBase } from '../common/validators';

const EmployeeValidator = {
  idEmploy: Joi.string()
    .trim()
    .required(),
  nameEmployee: Joi.string()
    .trim()
    .required(),
  year: Joi.number()
    // .min(userConstant.MIN_USER_AGE)
    .required(),
  address: Joi.string()
    .trim()
    .required(),
  phone: Joi.number()
    .min(employeeConstant.MIN_PHONE_NUMBER)
    // .regex(/^\d{3}-\d{3}-\d{4}$/)
    .required(),
  email: Joi.string()
    .trim()
    .email()
    .required()
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeValidator
})
  .required()
  .label('Response - Employee');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response - Employees');

const createEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - new data employee');

const updateEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request -  update data employee');
const deleteEmployeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('delete obejct data employee');

export {
  EmployeeResponseValidator,
  createEmployeeRequestValidator,
  EmployeeListResponseValidator,
  EmployeeValidator,
  updateEmployeeRequestValidator,
  deleteEmployeRequestValidator
};
