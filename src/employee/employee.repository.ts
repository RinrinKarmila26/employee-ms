import { EmployeeModel, EmployeeDocument } from './employee.model';

import { IEmployee, IEmployeeResponse } from './employee.interface';

const EmployeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const EmployeeDocumentsToObjects = (documents: EmployeeDocument[]) =>
  documents.map(EmployeeDocumentToObject);

const get = async () => {
  const documents = await EmployeeModel.find().exec();
  return EmployeeDocumentsToObjects(documents);
};

const create = async (employ: IEmployee) => {
  const newEmploy = new EmployeeModel(employ);

  await newEmploy.save();

  return EmployeeDocumentToObject(newEmploy);
};

const updateDataEmployee = async (employUpdate: IEmployee, _id: string) => {
  const updatedEmploye = new EmployeeModel(employUpdate);

  await EmployeeModel.findOneAndUpdate({ _id }, employUpdate, {
    new: true
  });

  return EmployeeDocumentToObject(updatedEmploye);
};

const deleteDataEmployee = async (_id: string) => {
  const dataDelet = new EmployeeModel(_id);
  await EmployeeModel.findOneAndDelete({ _id });
  return EmployeeDocumentToObject(dataDelet);
};

const getFirstById = async (idEmploy: string) => {
  const employ = await EmployeeModel.findOne({ idEmploy }).exec();
  return employ && EmployeeDocumentToObject(employ);
};

const employeeRepository = {
  get,
  create,
  updateDataEmployee,
  getFirstById,
  deleteDataEmployee
};
export default employeeRepository;
