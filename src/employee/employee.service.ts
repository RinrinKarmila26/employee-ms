import { ERROR_CODE } from '../common/errors';
import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';
// import { EmployeeModel } from './employee.model';
// import hapiswagger = require('hapi-swagger');

const getEmployees = () => {
  return employeeRepository.get();
};

const createEmployee = async (employ: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstById(
    employ.idEmploy
  );
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return employeeRepository.create(employ);
};

const updateEmployee = async (employUpdate: IEmployee, _id: string) => {
  return await employeeRepository.updateDataEmployee(employUpdate, _id);
};

const deleteEmploy = async (_id: string) => {
  return employeeRepository.deleteDataEmployee(_id);
};

const employeeService = {
  getEmployees,
  createEmployee,
  updateEmployee,
  deleteEmploy
};
export default employeeService;
