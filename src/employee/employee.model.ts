import mongoose, { Schema, Document, Model } from 'mongoose';
import { IEmployee } from './employee.interface';

export type EmployeeDocument = IEmployee & Document;

const EmployeeSchema: Schema = new Schema({
  idEmploy: {
    required: true,
    type: String
  },
  nameEmployee: {
    required: true,
    type: String
  },
  year: Number,
  country: {
    required: true,
    type: String
  },
  address: {
    required: true,
    type: String
  },
  phone: Number,
  email: {
    required: true,
    type: String
  },
  sosialMedia: []
});

export const EmployeeModel: Model<EmployeeDocument> = mongoose.model(
  'Employee',
  EmployeeSchema
);
