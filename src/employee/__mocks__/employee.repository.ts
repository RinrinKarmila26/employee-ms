export default {
  get: jest.fn(),
  create: jest.fn(),
  getFirstById: jest.fn(),
  updateDataEmployee: jest.fn(),
  deleteDataEmployee: jest.fn()
};
